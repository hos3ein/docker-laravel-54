<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="{{ route('people.index') }}">لیست افراد</a></li>
            <li><a class="nav-link" href="{{ route('people.create') }}">افزودن فرد جدید</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    @if(session()->has('message'))
        <div class="row" id="message">
            <div class="col-lg-12">
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session('message') }}
                </div>
            </div>
        </div>
    @endif

    @yield('content')
</div>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $('#message').delay(3000).slideUp();
    });
</script>
</body>
</html>