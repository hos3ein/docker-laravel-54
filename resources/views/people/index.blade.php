@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">لیست افراد</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>نام</th>
                                <th>نام خانوادگی</th>
                                <th>تلفن</th>
                                <th style="width: 123px">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($peoples as $people)
                                <tr data-id="{{ $people->id }}">
                                    <td>{{ ($peoples->currentpage()-1) * $peoples->perpage() + $loop->iteration }}</td>
                                    <td>{{ $people->name }}</td>
                                    <td>{{ $people->family }}</td>
                                    <td>{{ $people->phone }}</td>
                                    <td>
                                        <a href="{{route('people.edit', $people)}}"
                                           class="btn btn-info btn-sm">ویرایش</a>
                                        <a href="{{route('people.destroy', $people)}}"
                                           onclick="return confirm('آیا برای حذف مطمئن هستید؟')"
                                           class="btn btn-danger btn-sm">حذف</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="text-center">
                        {{ $peoples->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection