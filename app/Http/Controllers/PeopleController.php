<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    public function index()
    {
        $peoples = People::latest()->paginate(10);
        return view('people.index', compact('peoples'));
    }

    public function create()
    {
        return view('people.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'family' => 'required|max:100',
            'phone' => 'required|max:11',
        ]);

        $people = new People([
            'name' => $request->name,
            'family' => $request->family,
            'phone' => $request->phone,
        ]);
        $people->save();

        session()->flash('message', 'با موفقیت ثبت شد');
        return redirect(route('people.index'));
    }

    public function edit(People $people)
    {
        return view('people.edit', compact('people'));
    }

    public function update(Request $request, People $people)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'family' => 'required|max:100',
            'phone' => 'required|max:11',
        ]);

        $people->update([
            'name' => $request->name,
            'family' => $request->family,
            'phone' => $request->phone,
        ]);

        session()->flash('message', 'با موفقیت ویرایش شد');
        return redirect(route('people.index'));
    }

    public function destroy(People $people)
    {
        $people->delete();
        session()->flash('message', 'با موفقیت حذف شد');
        return redirect(route('people.index'));
    }
}
