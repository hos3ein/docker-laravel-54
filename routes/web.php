<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function (\Illuminate\Http\Request $request) {
    return '/?s=' . $request->get('s') . ' -> ' . env($request->get('s'));
});

Route::group(['prefix' => 'peoples', 'middleware' => ['web']], function () {

    Route::get('/', ['uses' => 'PeopleController@index', 'as' => 'people.index']);
    Route::get('/create', ['uses' => 'PeopleController@create', 'as' => 'people.create']);
    Route::post('/', ['uses' => 'PeopleController@store', 'as' => 'people.store']);
    Route::get('/{people}/edit', ['uses' => 'PeopleController@edit', 'as' => 'people.edit']);
    Route::patch('/{people}', ['uses' => 'PeopleController@update', 'as' => 'people.update']);
    Route::get('/{people}', ['uses' => 'PeopleController@destroy', 'as' => 'people.destroy']);

});